/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 

Курс валют НБУ з датою на який день, 

планет зоряних війн, 

список справ з https://jsonplaceholder.typicode.com/ 
//виводити які з можливістю редагування при натискані 
*/

const req = async (url) => {
    document.querySelector(".box_loader").classList.add("show")
    const data = await fetch(url);
    return await data.json();
}

const nav = document.querySelector(".nav")
    .addEventListener("click", (e) => {
        if (e.target.dataset.link === "nbu") {
            req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
                .then((info) => {
                    show(info)
                })
        } else if (e.target.dataset.link === "star") {
            document.querySelector("tbody").innerHTML =''
            // вивести всі 60 планет з https://swapi.dev/api/planets/
            req("https://swapi.dev/api/planets/?format=json") 
                .then((info) => {
                    shows(info) 
            })   
        } else if (e.target.dataset.link === "todo") {
            req("https://jsonplaceholder.typicode.com/todos")
                .then((info) => {
                    show(info)
            })
            
        } 
        else {new Error('Error')}  
    })


function show(data = []) {
    
    if (!Array.isArray(data)) return;
    const tbody = document.querySelector("tbody");
    tbody.innerHTML = "";

    const newArr = data.map(({txt, rate, exchangedate, title, completed}, i) => {
      
        return {
            id: i + 1,
            name : txt || title,
            info1 : rate || completed,
            info2 : exchangedate || "додайте значення"
        }
    });
    
    newArr.forEach(({ name, id, info1, info2 }) => {
        tbody.insertAdjacentHTML("beforeend", `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `)
    })
    
    tbody.addEventListener('click', (e)=>{
        e.target.innerHTML = prompt("Додайте значення", e.target.innerHTML)
    })
    document.querySelector(".box_loader").classList.remove("show")    
}

let count = 1
   function shows(data = []) {
        const tbody = document.querySelector("tbody")
        const arr = data.results
      
        arr.forEach(({name, climate, terrain}) => {
            tbody.insertAdjacentHTML('beforeend', `
            <tr> 
            <td>${count++}</td>
            <td>${name}</td>
            <td>${climate}</td>
            <td>${terrain}</td>
            </tr>
            `)        
        })
        
            if (data.next!==null){
            req([data.next])
            .then((info) => {
               shows(info)
            })
            .catch((error)=>console.error(error))    
            } else {
                document.querySelector(".box_loader").classList.remove("show")
            }

            if (count>=60){count=1}
    }
   