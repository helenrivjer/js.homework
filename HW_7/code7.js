/*Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
 - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, 
 за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], 
і другим аргументом передати 'string', то функція поверне масив [23, null]. 
*/


   function filterBy() {
        const arr = ['this', 'time', 'is', 3, 2, 7, null,'out'];
        const string = arr.filter((item) => typeof item === 'string');
        console.log(string)
        }
    filterBy()
  
    function filterBy2() {
        const arr = ['this', 'time', 'is', 3, 2, 7, null,'out'];
        const number = arr.filter((item) => typeof item === 'number');
        console.log(number)
        }
    filterBy2()


 
 //2.Задача 2 Переписати гру шибениця з книги на новий синтасис.
 let words = [
 "программа",
 "макака",
 "прекрасный",
 "оладушек"
 ];
 const word = words[Math.floor(Math.random() * words.length)];
 const answerArray = [];
 for (let i = 0; i < word.length; i++) {
 answerArray[i] = "_";
 }
 const remainingLetters = word.length;
 // Игровой цикл
 while (remainingLetters > 0) {
 alert(answerArray.join(" "));
 // Запрашиваем вариант ответа
 const guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
 if (guess === null) {
 // Выходим из игрового цикла
 break;
 } else if (guess.length !== 1) {
 alert("Пожалуйста, введите одиночную букву.");
 } else {
 // Обновляем состояние игры
 for (let j = 0; j < word.length; j++) {
 if (word[j] === guess) {
 answerArray[j] = guess;
 remainingLetters--;
 }
 }
 }
}
alert(answerArray.join(" "));
alert(`Отлично! Было загадано слово " ${word}`)


