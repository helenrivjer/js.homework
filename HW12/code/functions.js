import { pizzaSelectUser } from "./index.js";
import pizza from "./pizza.js";

    function userSlectTopping(topping) {
        if ("smallmidbig".includes(topping)) {
            pizzaSelectUser.size = pizza.size.find((el) => {
                return el.name === topping
            })
        } else if ("moc1moc2moc3sprinklesstarpalka".includes(topping)) {
            pizzaSelectUser.topping.push(pizza.topping.find(el => el.name === topping))   
           
        } else if ("bluegreenblack".includes(topping)) {
            pizzaSelectUser.sauce.push(pizza.sauce.find(el => el.name === topping))
        }
        pizzaSelectUser.price = show(pizzaSelectUser);
    }

    function userRemoveTopping(topping) {
        if ("moc1moc2moc3sprinklesstarpalka".includes(topping)) {
            pizzaSelectUser.topping = [...pizzaSelectUser.topping.filter(el => el.name !== topping)]
        } else if ("bluegreenblack".includes(topping)) {
            pizzaSelectUser.sauce = [...pizzaSelectUser.sauce.filter(el => el.name !== topping)]
        }
        pizzaSelectUser.price = show(pizzaSelectUser);
    }
    function show(pizza) {
        let price = 0;
        if (pizza.sauce.length > 0) {
            price += pizza.sauce.reduce((a,b)=>{
                return a + b.price
            }, 0)
        }
          
        if(pizza.topping.length > 0){
            price += pizza.topping.reduce((a,b)=>{
                return a + b.price
            }, 0)

        }
        if(pizza.size !== ""){
            price += pizza.size.price;
        }    
         console.log(price);  
    
        return price;
    }

    function display () {
            let toppingName = document.getElementById('topping')
            let sauceName = document.getElementById('sauce')
            toppingName.innerHTML = ''
            sauceName.innerHTML = ''
            document.getElementById("price").innerHTML = pizzaSelectUser.price;
            createDeleteElement(pizzaSelectUser.topping, toppingName)
            createDeleteElement(pizzaSelectUser.sauce, sauceName)
    }
       
    function createDeleteElement (topping, parent) {
            
        topping.forEach(e=> {
            const div = document.createElement('div')
            div.innerHTML = e.productName
            div.setAttribute("data-id", e.name)
            parent.append(div)
            const inp = document.createElement('input')
            inp.setAttribute('type', 'button')
            inp.setAttribute('id', 'del')
            inp.setAttribute('value', 'X')
            div.appendChild(inp)
                   
            inp.addEventListener('click', (e)=>{
                const a = document.querySelector('.draggable')
                let id = div.dataset.id;
                userRemoveTopping(id);
                display();
                removeToppingImg (id);
                e.target.remove()
            })
        }) 
        }
      
    function removeToppingImg (id) {
        let toppingImages = document.querySelectorAll(`#${id}`)
        if(toppingImages.length){
        toppingImages[0].remove()
        }
    }
   
export { userSlectTopping, display, removeToppingImg, show }