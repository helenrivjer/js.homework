const pizza = {
    size : [
        {name : "small", price: 40},
        {name : "mid",   price: 55},
        {name : "big",   price: 70}
    ],
    topping: [
        {name : "moc1",      price: 10, productName: "Сонячний гліттер"},
        {name : "moc2",      price: 25, productName: "Рожевий ранок"},
        {name : "moc3",      price: 15, productName: "Небесна блакить"},
        {name : "sprinkles", price: 20, productName: "Райдужні кульки"},
        {name : "star",      price: 30, productName: "Міжгалактичні сузір'я"},
        {name : "palka",     price: 35, productName: "Ебонітові палички"},
        
    ],
    sauce: [
        {name: "blue",  price : 20, productName: "Ocean breeze"},
        {name: "green", price : 22, productName: "Juicy green"},
        {name: "black", price : 24, productName: "Mysterious gothic"}
    ]
}

export default pizza;