import {clickInputSize, clickSauceAdd, clickToppingAdd} from "./functionEvent.js"
import {display, userSlectTopping} from "./functions.js"

document.getElementById("pizza")
    .addEventListener("click", clickInputSize);
/*
document.querySelectorAll(".topping")
.forEach((div)=>{
    div.addEventListener("click", clickToppingAdd)
})

document.querySelectorAll(".sauce")
.forEach((div)=>{
    div.addEventListener("click", clickSauceAdd)
})
*/

function f() {
    const ingridients = document.querySelectorAll('.draggable');
    
       ingridients.forEach((div)=>{div.addEventListener('dragstart', function (evt) {
        //this.style.border = "3px dotted #000"
        evt.dataTransfer.effectAllowed = "move";
        evt.dataTransfer.setData("Text", this.id);
    }, false)
    })
    
    ingridients.forEach((div)=>{div.addEventListener("dragend", function (evt) {
        this.style.border = ''      
    }, false);
    })

    const table = document.getElementById("t");
    table.addEventListener("dragenter", function (evt) {
        this.style.border = "3px solid red";
    }, false);
    
    table.addEventListener("dragover", function (evt) {
        if (evt.preventDefault) evt.preventDefault();
        return false;
    }, false);

    table.addEventListener("drop", function (evt) {
        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        this.style.border = "";
        let id = evt.dataTransfer.getData("Text")
        console.log(id)
        let data = document.getElementById(id)
        let img = document.createElement('img')
        img.src = data.src
        let elem = document.getElementById(id).cloneNode(true);                 
        this.appendChild(elem);
       // this.appendChild(img)
        userSlectTopping(id)
        display()
        return false;    
    }, false);        
}
f()
export const pizzaSelectUser = {
   size : "",
   topping : [],
   sauce : [],
   price : 0
}

document.getElementById('banner').
addEventListener('mouseover', (e) => {
    e.target.style.right = Math.random() * 80 + "%";
    e.target.style.bottom = Math.random() * 80 + "%";
})

const name = document.getElementById('name')
    name.addEventListener('input', (e)=>{  
    let pattern = /[а-яА-Яїіє]{2}/
    if (pattern.test(e.target.value)){
    name.style.border = '5px solid green'}
    else {name.style.border = '5px solid red'}
})
const tel = document.getElementById('tel')
    tel.addEventListener('input', (e)=>{  
    let pattern = /\+380\d{9}$/
    if (pattern.test(e.target.value)){
    tel.style.border = '5px solid green'}
    else {tel.style.border = '5px solid red'}
})
const mail = document.getElementById('mail')
    mail.addEventListener('input', (e)=>{  
    let pattern = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i;
    if (pattern.test(e.target.value)){
    mail.style.border = '5px solid green'}
    else {mail.style.border = '5px solid red'}
})      
