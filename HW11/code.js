/*Завдання №1
Прив'яжіть усім інпутам наступну подію - втрата фокусу, кожен інпут виводить своє value в параграф з id="test".

Завдання №2
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. 
Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною. 
*/
const [...inputs] = document.querySelectorAll('input')

inputs.forEach((inp)=>{
    inp.setAttribute('data-length', '5')
    inp.addEventListener('blur', () => {
        const a = document.createElement('p')
        a.setAttribute('id', 'test')
        a.innerText = inp.value
        console.log(a)                                                     //value в параграф з id="test" виводиться в консолі

        if (inp.value.length === parseInt(inp.attributes[2].nodeValue)){   //перевірка на кількість введених символів
            inp.classList.add('green')
        }
        else{
            inp.classList.add('red')
        }
    })

    inp.addEventListener('change', () => {
        inp.classList.remove('red')
        inp.classList.remove('green')
        inp.value.length === parseInt(inp.attributes[2].nodeValue) ? inp.classList.add('green') : inp.classList.add('red')
    })
})

/*
Завдання №3
При завантаженні сторінки показати користувачеві поле введення (input) з написом Price. Це поле буде служити для введення числових значень. 
Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється span, в якому має бути виведений текст: 
  - Поруч із ним має бути кнопка з хрестиком (X). Значення всередині поля введення фарбується зеленим.
  - При натисканні на Х - span з текстом та кнопка X повинні бути видалені.
  - Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу - Please enter correct price. span зі значенням при цьому не створюється.
*/
window.addEventListener("DOMContentLoaded", () => {
    const price = document.createElement('input')
    price.setAttribute('type', 'number') 
    price.placeholder = "Price"
    inputs[0].insertAdjacentElement('beforebegin', price)

    price.classList.add('first')
    const p = document.createElement('p')
    price.insertAdjacentElement('afterend', p)

    price.addEventListener('focus', () => {                   //При фокусі на полі введення – з'являється рамка зеленого кольору
        price.classList.add('focus')
    })
    price.addEventListener('blur', () => {
        price.classList.remove('focus')
        const span = document.createElement('span')           //Коли забрали фокус з поля - його значення зчитується, над полем створюється span
        span.innerText = `Встановлена ціна: ${price.value} $`
        price.insertAdjacentElement('beforebegin', span) 
        const btn = document.createElement('input')           //кнопка з хрестиком (X)
        btn.setAttribute("type", 'button')
        btn.setAttribute('value', "X")
        span.append(btn)                               

        btn.addEventListener('click', () => {
        span.remove(price)
        })
        
        if(price.value < 0) {                                 //Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу - Please enter correct price. span зі значенням при цьому не створюється.
            price.classList.add('red')
            p.innerText = 'Please enter correct price'
            span.remove(price)
        }
        else{
            p.innerText = ''
        }
        price.addEventListener('change', () => {              //При зміні введеного значення в спані видаляється старе значення і потім записується нове
             price.classList.remove('red')
             span.innerText = ''
        })
    })
    price.addEventListener('input', () => {
        price.classList.remove('focus')
        price.style.background = 'beige'
        price.style.color = 'green'                           // Значення всередині поля введення фарбується зеленим.
    })
})