//Виконати запит на https://swapi.dev/api/people (https://starwars-visualguide.com) отримати список героїв зіркових воєн. 
//Вивести кожного героя окремою карткою із зазначенням. картинки Імені, статевої приналежності, ріст, колір шкіри, рік народження та планету на якій народився. 
//Створити кнопку зберегти на кожній картці. При натисканні кнопки записуйте інформацію у браузері
const main = document.querySelector('main')
const container = document.querySelector('container')
const loader = document.querySelector(".box-loader")

function getServer (url, callback) {
    loader.classList.add("show")
    const ajax = new XMLHttpRequest();
    ajax.open("GET", url);
    ajax.send();
    ajax.addEventListener('readystatechange', ()=>{
          console.log(ajax.readyState)
          if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
                  callback(JSON.parse(ajax.response))
                  loader.classList.remove("show")
          }else if (ajax.readyState === 4) {
                throw new Error(`Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`)
          }
    })
}
       
function show (data =[]) {
    const rez = data.results
    rez.forEach((results, index) => {
    
        const person = `<section>
        <div id = "picture"><img src="https://starwars-visualguide.com/assets/img/characters/${index+1}.jpg" alt="${index+1}"></div>
        <div id = 'name'>Name: ${results.name}</div>
        <div id = 'height'>Height: ${results.height}</div>
        <div id = 'mass'>Mass: ${results.mass}</div>
        <div id = 'gender'>Gender: ${results.gender}</div>
        <div id = 'skin_color'>Skin color: ${results.skin_color}</div>
        <input type = 'button' id = '${index+1}' value = 'save'>
        </section>`

    main.insertAdjacentHTML('beforeend', person)
    
    const a = {
        picture: `<img src="https://starwars-visualguide.com/assets/img/characters/${index+1}.jpg" alt="${index+1}"`,
        name: results.name,
        heigh: results.height,
        mass: results.mass,
        gender: results.gender,
        homeworld: results.homeworld
    }
    const inp = document.getElementById(`${index+1}`)
    inp.addEventListener('click', ()=>{
        console.log(a)
    })
    const homeworld = document.createElement('div')   
    homeworld.setAttribute('id', 'home') 
    inp.before(homeworld)
    console.log(homeworld)

    function s (planet){
        homeworld.innerText= `Homeworld: ${planet.name}`
        return planet.name
    }

    getServer(results.homeworld, s) 

    })   
}
getServer('https://swapi.dev/api/people/', show)

        

//Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/ Сайт має виглядати так : https://kondrashov.online/images/screens/120.png 
//На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні сторінок показувати нові коментарі.  з коментарів виводити :  "id": 1, "name" "email" "body":
const currentDate = `${new Date().getFullYear()}-${new Date().getMonth() < 10 ? `0${new Date().getMonth() + 1}` : new Date().getMonth() + 1}-${new Date().getDate() < 10 ? `0${new Date().getDate()}` : new Date().getDate()}`;

function showComments (data =[]) {
        let currentPage = 1;
        let rows = 10;
        function displayList(arrData, rowPerPage, page) {
            const postsEl = document.querySelector('.posts');
            postsEl.innerHTML = "";
            page--; 
            const start = rowPerPage * page;
            const end = start + rowPerPage;
            const data = arrData.slice(start, end);

            data.forEach(({name, id, email, body}) => {
                const comments = `<div id = 'block'>
                <div>${id}. ${name}</div>
                <div>${currentDate}</div>
                <div>${body}</div>
                <div><a href="mailto:${email}">${email}</a></div></div>`
               container.append(postsEl)
               postsEl.insertAdjacentHTML('beforeend',comments)
            })
        }
        
         
        function displayPagination(arrData, rowPerPage) {
              const paginationEl = document.querySelector('.pagination');
              const pagesCount = Math.ceil(arrData.length / rowPerPage);
              const ulEl = document.createElement("ul");
              ulEl.classList.add('pagination__list');
      
          for (let i = 0; i < pagesCount; i++) {
              const liEl = displayPaginationBtn(i + 1);
              ulEl.append(liEl)
          }
          container.after(paginationEl)
          paginationEl.append(ulEl)
         
        }
        
        function displayPaginationBtn(page) {
          const liEl = document.createElement("li");
          liEl.classList.add('pagination__item')
          liEl.innerText = page
          if (currentPage == page) liEl.classList.add('pagination__item--active')
         
      
          liEl.addEventListener('click', () => {
            currentPage = page
            displayList(data, rows, currentPage)
      
            let currentItemLi = document.querySelector('li.pagination__item--active');
            currentItemLi.classList.remove('pagination__item--active');
            liEl.classList.add('pagination__item--active');
          })
      
          return liEl;
        }
        displayList(data, rows, currentPage)
        displayPagination(data, rows);  
        
    }  
          
    getServer('https://jsonplaceholder.typicode.com/comments', showComments)