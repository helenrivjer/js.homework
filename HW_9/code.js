/*Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло 
і використовуючи document.location переведіть користувача на сторінку 
https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.       
*/  

window.onload = () => {   
    
        const tel = document.createElement('input');
        tel.classList.add('checkTel');
        document.body.insertAdjacentElement('afterend', tel);
        tel.placeholder = '000-000-00-00';
        const n = document.createElement('div');
        n.style.marginLeft = '.5rem'
        n.innerText = 'Невірний формат';

        const save = document.createElement('input');
        save.setAttribute('type', 'button')
        save.setAttribute('value', 'Зберегти')
        tel.insertAdjacentElement('afterend', save)
      
        
        let pat = /\d{3}-\d{3}-\d{2}-\d{2}$/;
        
        save.onclick = () =>{
            tel.classList.remove('error')
            if (pat.exec(tel.value) !==null){
                setTimeout(() => {
                    document.location='https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg'    
                }, 2000);
                n.innerText = '';
                document.body.classList.add('green')    
            }
                else{
                    save.insertAdjacentElement('afterend', n)
                    tel.classList.add('error')
                }
        }
       
/*
2.Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

            let counter = 0, counter2 = 0, counter3 = 0, intervalHandler, flag = false;
            const s = document.querySelectorAll(".stopwatch-display span");

            const sec = () => {
                counter++;
                s[2].innerText = counter <= 9 ? '0'+counter : counter;   
                if(counter >= 60){
                    s[2].innerText = '00';
                    counter = 0;  
                    counter2++;  
                    s[1].textContent = counter2 <= 9 ? '0'+counter2 : counter2;
                }    
                if(counter2 >= 60){
                   s[1].innerText = '00';
                   counter2 = 0;
                   counter3++;
                   s[0].textContent = counter3 <= 9 ? '0'+counter3 : counter3;
                }    
                if(counter3 >= 60){
                        clearInterval(intervalHandler); 
                }    
            }    
          
    const b = document.querySelectorAll('.container-stopwatch'); 
           
    const btn = document.querySelectorAll('.stopwatch-control button');
        btn.forEach((el) => {
        el.onclick = () => {
            console.log(el.innerText);
            if(!flag && el.innerText === 'START'){
                intervalHandler = setInterval(sec, 1000);
                b[0].classList.add('green')
                flag = true
            }
            else if(el.innerText === 'STOP'){
                clearInterval(intervalHandler);
                b[0].classList.remove('green');
                b[0].classList.add('red');
                flag = false
            }
            else if(el.innerText === 'RESET'){
                counter = 0; counter2 = 0; counter3 = 0;
                s[0].innerText = '00';
                s[1].innerText = '00';
                s[2].innerText = '00';
                b[0].classList.remove('green');
                b[0].classList.remove('red');
                b[0].classList.add('silver');
                flag = false   
            }
        }
    })
   
}

