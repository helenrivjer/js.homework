/*Згенерувати теги через javascript. Додати на сторінку семантичні теги та метагеги опису сторінки.
 прописати стилі для для елементів використовуючи css id та класи
 при натиску на тег ми можемо доти будь-який контент і він зберігається в тегу
 */

document.body.innerText = 'BODY';

const header = document.createElement('header');
header.classList.add('headerMain');
document.body.insertAdjacentElement('afterend', header);

const headerInput = document.createElement('input');
headerInput.placeholder = 'HEADER';
header.appendChild(headerInput)

const nav = document.createElement('nav');
nav.classList.add('navMain');
header.appendChild(nav)

const inputNav = document.createElement('input');
inputNav.placeholder = 'NAV';
nav.appendChild(inputNav)

const section = document.createElement('section');
section.classList.add('section-left');
nav.insertAdjacentElement('afterend', section)

const inputSection = document.createElement('input');
inputSection.placeholder = 'SECTION';
section.appendChild(inputSection);

const headerSection = document.createElement('header');
headerSection.classList.add('headerSection');
section.appendChild(headerSection);

const headerSectionInput = document.createElement('input');
headerSectionInput.placeholder = 'HEADER';
headerSection.appendChild(headerSectionInput)

const section2 = document.createElement('section');
section2.classList.add('section-right');
nav.insertAdjacentElement('afterend', section2)

const inputSection2 = document.createElement('input');
inputSection2.placeholder = 'SECTION';
section2.appendChild(inputSection2);

const headerSection2 = document.createElement('header');
headerSection2.classList.add('headerSection2');
section2.appendChild(headerSection2);

const headerSectionInput2 = document.createElement('input');
headerSectionInput2.classList.add('inputSection');
headerSectionInput2.placeholder = 'HEADER';
headerSection2.appendChild(headerSectionInput2)

const nav2 = document.createElement('nav');
nav2.classList.add('nav2');
headerSection2.appendChild(nav2)

const inputNav2 = document.createElement('input');
inputNav2.placeholder = 'NAV';
nav2.appendChild(inputNav2)

//
const article = document.createElement('article');
article.classList.add('article1');
section.append(article)
const inputArticle = document.createElement('input');
inputArticle.placeholder = 'ARTICLE';
article.append(inputArticle)

const article2 = document.createElement('article');
article2.classList.add('article2');
section.append(article2);
const inputArticle2 = document.createElement('input');
inputArticle2.placeholder = 'ARTICLE';
article2.append(inputArticle2)

let art = {
    articleMain1: {
        header: document.createElement('header'),
        p: document.createElement('p'),
        p1: document.createElement('p'),
        aside: document.createElement('aside'),
        footer: document.createElement('footer'),
        },
    articleMain2: {
            header: document.createElement('header'),
            p: document.createElement('p'),
            p1: document.createElement('p'),
            footer: document.createElement('footer'),
            }
}
         for (let element in art.articleMain1) {
          section.appendChild(art.articleMain1[element]);
          const input = document.createElement('input');
         art.articleMain1[element].appendChild(input)
         article.append(art.articleMain1[element])
         }
         
         for (let element in art.articleMain2) {
            section.appendChild(art.articleMain2[element]);
            const input = document.createElement('input');
           art.articleMain2[element].appendChild(input)
           article2.append(art.articleMain2[element])
           }

        const footer = document.createElement('footer');
        footer.classList.add('footer');
        section.appendChild(footer);
        const inputFooter2 = document.createElement('input');
        inputFooter2.placeholder = 'FOOTER';
        footer.append(inputFooter2)

        
         art.articleMain1.header.childNodes[0].placeholder = 'HEADER'
         art.articleMain1.p.childNodes[0].placeholder = 'P'
         art.articleMain1.p1.childNodes[0].placeholder = 'P'
         art.articleMain1.aside.childNodes[0].placeholder = 'ASIDE'
         art.articleMain1.footer.childNodes[0].placeholder = 'FOOTER'
         
         art.articleMain2.header.childNodes[0].placeholder = 'HEADER'
         art.articleMain2.p.childNodes[0].placeholder = 'P'
         art.articleMain2.p1.childNodes[0].placeholder = 'P'
         art.articleMain2.footer.childNodes[0].placeholder = 'FOOTER'

const footerBottom = document.createElement('footer');
footerBottom.classList.add('footerBottom');
section.insertAdjacentElement('afterend', footerBottom);

const inputfooterBottom = document.createElement('input');
inputfooterBottom.placeholder = 'FOOTER';
footerBottom.appendChild(inputfooterBottom)
      
       
 /*        
Пробувала спершу заповнювати теги без додавання імпутів до кожного з них, цим методом:
document.addEventListener('click', function(event){
    let element = prompt('введіть текст')
    event.target.childNodes[0].textContent = element
})

І це було найлегше. Але викликати imput по клікові, а не prompt, так і не вдалося. І чи можливо це було взагалі?
*/

