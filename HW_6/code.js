/*Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
*/
class Worker {
    constructor(name, surname, rate, days){
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days
    }
}

let person = new Worker('Homer', 'Simpson', 1001, 25);
document.write(person.name + '<br>' + person.surname + '<br>');
Worker.prototype.getSelary = function (){
    return this.rate*this.days
}

document.write(person.getSelary());
document.write('<hr>')

/*Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. 
Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону.
Викликати ці методи кожного з об'єктів.
*/

class Phone {
    constructor(number, model, weight){
        this.number = number;
        this.model = model;
        this.weight = weight;
    }

reciveCall(name){
   this.name = name;
    console.log(`Телефонує ${this.name}`)
}
getNumber(){
    return this.number
}
}

const nokia = new Phone('057752357', 'A250', '105');
console.log(`Номер: ${nokia.number}, модель: ${nokia.model}, вага: ${nokia.weight}`);
nokia.reciveCall('any name');
console.log(nokia.getNumber());

const samsung = new Phone('0364447114', 'S21', '111');
console.log(`Номер: ${samsung.number}, модель: ${samsung.model}, вага: ${samsung.weight}`);
samsung.reciveCall('somebody');
console.log(samsung.getNumber())

const pixel = new Phone('0230250258', 'v1', 99);
console.log(`Номер: ${pixel.number}, модель: ${pixel.model}, вага: ${pixel.weight}`);
pixel.reciveCall('nobody');
console.log(pixel.getNumber())

/*
Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.
*/
class MyString {
    constructor(){
this.name = 'Hello world'
    }
}
MyString.prototype.reverse = function () {
    document.write(this.name.split('').reverse().join(' ') + '<br>')
}
MyString.prototype.ucFirst = function () {
    document.write(this.name.charAt(0).toUpperCase() + this.name.slice(1) + '<br>')
}
MyString.prototype.ucWords = function () {
    document.write(this.name[0].toUpperCase() + this.name.slice(1,5) + ' ') 
    document.write(this.name[6].toUpperCase() + this.name.slice(7) + '<br>')}
   
const st = new MyString
st.reverse()
st.ucFirst()
st.ucWords()

document.write('<hr>')

//Варіант 2. 
//ucFirst, ucWords очевидно на строці викликати не можна

const myString = new String('hello simpsons')


function reverse(myString){
    document.write(myString.split('').reverse().join(' ') + '<br>')}
    reverse(myString)
function ucFirst(myString){
    document.write(myString.charAt(0).toUpperCase() + myString.slice(1) + '<br>')}
    ucFirst(myString)
function ucWords(myString) {
    document.write(myString[0].toUpperCase() + myString.slice(1,5) + ' ') 
    document.write(myString[6].toUpperCase() + myString.slice(7) + '<br>')}
ucWords(myString)    

document.write('<hr>')


/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". 
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/

class Engine{
    constructor(power, company){
        this.power = power,
        this.company = company
    }
}

class Driver extends Engine{
    constructor(sname, experience, power, company){
        super(power, company)
        this.sname = sname,
        this.experience = experience
    }
}
class Car extends Driver{
    constructor(model, carClass, weight, sname, experience, power, company){
        super(sname, experience, power, company)
        this.model = model,
        this.carClass = carClass,
        this.weight = weight
        
    }
    start(){
        console.log('Поїхали')
    }
    stop(){
        console.log('Зупиняємося')
    }
    turnRight(){
        console.log('Поворот праворуч')
    }
    turnLeft(){
        console.log('Поворот ліворуч')
    }
    toString(){
        console.log(`
        Ім'я: ${this.sname}, Досвід: ${this.experience}, Модель: ${this.model}, 
        Клас: ${this.carClass}, Вага: ${this.weight}, Потужність: ${this.power}, Виробник: ${this.company}`)
    }
}

const en = new Engine('110', 'NY')
const dr1 = new Driver('Oleh', 23, '100', 'WI')
const ford = new Car('2', '54', '500', dr1.sname, dr1.experience, en.power, en.company)


console.log(ford)
console.log(en)
console.log(dr1)
ford.start()
ford.stop()
ford.turnRight()
ford.turnLeft()
ford.toString()

class Lorry extends Car{
    constructor(model, carClass, weight, sname, experience, power, company){
        super(model, carClass, weight, sname, experience, power, company)
        this.carrying = 1000
    }
    toString(){
        console.log(`
        Ім'я: ${this.sname}, Досвід: ${this.experience}, Модель: ${this.model}, 
        Клас: ${this.carClass}, Вага: ${this.weight}, Потужність: ${this.power}, 
        Виробник: ${this.company}, Вантажопідйомність кузова: ${this.carrying}`)
    }
}
const lorry122 = new Lorry(1, 2, 3, 4, 5, 6, 7)
lorry122.toString()
console.log(lorry122)

class SportCar extends Car{
    constructor(model, carClass, weight, sname, experience, power, company, speed){
        super(model, carClass, weight, sname, experience, power, company)
        this.speed = speed
    }
}

const spcar = new SportCar('AA', '000', 500, 'Ivanov Ivan', 3, 120, 'some company')
console.log(spcar)
spcar.toString()
console.log(spcar.speed = 180)
console.log('Нарешті ця домашка закінчилася')
