/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. 
Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

const calculator = {
    firstNum: "",
    secondNum: "",
    sign: "",
    memory: "0"
}
a = true
window.addEventListener("DOMContentLoaded", function () {
    const btn = document.getElementById('key')
    btn.addEventListener('click', (e) => {
        if (validate(/\d/, e.target.value) && calculator.sign ==="") {
            calculator.firstNum += e.target.value;
            show(calculator.firstNum)
            a = true
        } else if (validate(/[+-/*]/, e.target.value)) {
            calculator.sign = e.target.value
        } else if (validate(/\d/, e.target.value) && calculator.sign !=="" && calculator.firstNum !== "") {
            calculator.secondNum +=e.target.value;
            show(calculator.secondNum);
            document.getElementById('equal').disabled = false 
            a = true 
        } else if (validate(/C/, e.target.value)) {
            calculator.firstNum = '';
            calculator.secondNum = '';
            calculator.sign = '';
            a = true
            show('0');

        } else if (validate(/mp/, e.target.value)) {
            calculator.secondNum = ''
            calculator.sign = ''
            calculator.memory = calculator.firstNum
            a = true
            showMemory('m')
        } else if (validate(/mn/, e.target.value)) {
            calculator.memory = '0'
            showMemory('m')
        } else if (validate(/[mrc]/, e.target.value) && a !==false ) {
            calculator.secondNum = ''
            calculator.firstNum !== '' && calculator.sign !== '' ? (calculator.secondNum = calculator.memory) : (calculator.firstNum = calculator.memory)
            a = false
            show(calculator.memory)
        }else if (validate(/[mrc]/, e.target.value) && a == false){
            calculator.memory = '0'
            calculator.firstNum = ''
            calculator.secondNum = ''
            show(calculator.memory)
        } else {
            
        if (calculator.sign === '+') {
            calculator.firstNum = ((+calculator.firstNum) + (+calculator.secondNum));
            calculator.secondNum = ''    
        }
        if (calculator.sign === '-'){
            calculator.firstNum = (calculator.firstNum - calculator.secondNum)
            calculator.secondNum = ''
        }
        if (calculator.sign === '/'){
            calculator.firstNum = (calculator.firstNum / calculator.secondNum).toFixed(2)
            if(calculator.secondNum === '0'){
                calculator.firstNum = 'error'
            }
            calculator.secondNum = ''    
        }
        if (calculator.sign === '*'){
            calculator.firstNum = (calculator.firstNum * calculator.secondNum)
            calculator.secondNum = ''
        }              
        show(calculator.firstNum)
    }
        console.log(calculator)
    })


const validate = (p, v) => p.test(v)
function show(res){
    const display = document.querySelector(".display > input");
    display.classList.remove('mr');
    display.value = res    
}
function showMemory(res){
    const display = document.querySelector(".display > input");
    display.classList.add('mr');
    display.value = res
}

}, false)
